<?php

/**
 * Form callback for settings.
 */
function civicrm_api_settings_form() {
  $form['civicrm_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviCRM Base URL'),
    '#default_value' => variable_get('civicrm_api_url',''),
  );
  $form['civicrm_api_sitekey'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviCRM Site Key'),
    '#description' => t('Matches <tt>CIVICRM_SITE_KEY</tt> in <tt>civicrm.settings.php</tt> on the remote server.'),
    '#default_value' => variable_get('civicrm_api_sitekey',''),
  );
  $form['civicrm_api_userkey'] = array(
    '#type' => 'textfield',
    '#title' => t('CiviCRM API Key'),
    '#description' => t('API key for user account with CiviCRM permissions on the remote server.'),
    '#default_value' => variable_get('civicrm_api_userkey',''),
  );
  return system_settings_form($form);
}

function civicrm_api_test_create_mailing_form() {
//  $api = _civicrm_api_init();
//  $params = array('group_type' => 2);
//  $api->Group->Get();
//  dpm(print_r($api,1), 'groups');
  $form['subject'] = array(
    '#type' => 'textfield',
    '#value' => 'Test mailing at ' . date('H:i'),
    '#title' => 'Subject',
  );
  $form['group'] = array(
    '#type' => 'select',
    '#options' => array(
        129 => 'Mail Debuggers',
      ),
    '#default_value' => 129,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function civicrm_api_test_create_mailing_form_submit($form, &$form_state) {
  global $user;
  dpm($form_state, 'fstate');
  $api = _civicrm_api_init();
  $params = array(
    'first_name' => 'abc3',
    'last_name' => 'xyz3',
    'contact_type' => 'Individual',
    'email' => 'man3@yahoo.com',
    'version' => 3,
  );
  $api->Contact->Create($params);
  $params = array(
    // Basic mail settings.
    'mailing_name'            => 'Test Mailing from ' . $user->uid,
    'groups'          => array(
      'include' => array($form_state['values']['group']),
    ),
    'mailings'        => array(),
    'created_id'      => 6435, // cid of email creator
    'subject'         => 'Test Mailing from ' . $user->uid,
    'body_html'       => '<b>Body HTML</b>',
    'body_text'       => 'Body text',
    'contact_id'      => 6435,
    'version'         => 3,
    'scheduled_id'    => 6435,
  );
  $api->Mailing->Create($params);
  dpm(print_r($api,1), 'api');
}

function civicrm_api_test_create_contact_form() {

}

/**
 * Test
 */
function _civicrm_api_ping() {
  if ($api = _civicrm_api_init()) {
    error_log(print_r($api,1));
    $api->Contact->Ping();
  }
}


/**
 * Test
 */
function _civicrm_api_test() {
  if ($api = _civicrm_api_init()) {
    error_log('test');
    if ($api->Contact->Get(6435)) {
    error_log(print_r($api,1));
    error_log(print_r($api->count,1));
    drupal_set_message('xx');
      drupal_set_message($api->count, 'contacts found'); // each key of the result array is an attribute of the api
      foreach ($api->values as $c) {
          drupal_set_message("\n".$c->sort_name. " working for ". $c->current_employer, 'employ');
       }
     }
     else { // in theory, doesn't append
      drupal_set_message(t('CiviCRM API Error: %errmsg', array('%errmsg' => $api->errorMsg())), 'error');
    }
  }
}

/**
 * Test.
 */
function civicrm_api_test() {
  if ($api = _civicrm_api_init()) {
    drupal_set_message('civicrm api initialised');
    $contact_search_params = array(
      'contact_type' => 'Individual',
      'return'       => 'sort_name,email',
      'first_name'   => 'Christopher',
      'last_name'    => 'Burgess',
      'email'        => 'chris@giantrobot.co.nz',
    );
    dpm($contact_search_params, 'contact params');
    if ($api->Contact->Get($contact_search_params)) {
      // if we haven't matched exactly one person,
      // create a new contact
      if ($api->count == 1) {
        dpm($api->values, 'contact');
      }
    }
    else {
      drupal_set_message($api->errorMsg());
    }
  }
  else {
    drupal_set_message('civicrm api failed');
  }
}
